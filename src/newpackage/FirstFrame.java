package newpackage;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class FirstFrame extends JFrame {
    private final ArrayList<Point> coordinateList = new ArrayList<>();
    private int pointNumber = 1;
    
    JButton introdPctMouse;
    JButton introdPctCoord;
    JButton sortare;
    JButton acoperire;
    
    private boolean acop;
    
    //dimensiuni panel
    int xPanel = 400;
    int yPanel = 400;
    
    public FirstFrame() {
        setTitle("Acoperirea convexa");
        setSize(300,400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        
        //creare butoane
        introdPctMouse = new JButton("Introduceti punctele (click)");
        introdPctCoord = new JButton("Introduceti punctele (coordonate)");
        sortare = new JButton("Sortare puncte");
        acoperire = new JButton("Acoperirea convexa");
        
        introdPctMouse.setEnabled(true);
        introdPctCoord.setEnabled(true);
        sortare.setEnabled(false);
        acoperire.setEnabled(false);
        
        introdPctMouse.addActionListener(new IntrodListener());
        introdPctCoord.addActionListener(new IntrodListenerCoord());
        sortare.addActionListener(new SortareListener());
        acoperire.addActionListener(new AcoperireListener());
        
        GridLayout gridLayout = new GridLayout(4,1,50,50);
        setLayout(gridLayout);
        add(introdPctMouse);
        add(introdPctCoord);
        add(sortare);
        add(acoperire);
        
        setVisible(true);
    }
    
    
    class IntrodListenerCoord implements ActionListener {
        private int x,y;
        
        @Override
        public void actionPerformed(ActionEvent e) {
            if (acop) {
                coordinateList.clear();
                acop = false;
            }
            
            JFrame frame = new JFrame();
            frame.setTitle("Introduceti coordonatele punctelor (-200,200) in pixeli");
            frame.setSize(200,200);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            
            JTextField xCoord = new JTextField(0 + "",10);
            JTextField yCoord = new JTextField(0 + "",10);
            JButton ok = new JButton("OK");
            
            
            GridLayout layout = new GridLayout(3,1,10,10);
            frame.setLayout(layout);
            frame.add(xCoord);
            frame.add(yCoord);
            frame.add(ok);
            
            ok.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        x = Integer.parseInt(xCoord.getText());
                        y = Integer.parseInt(yCoord.getText());
                        if ( (Math.abs(x) > 200) || (Math.abs(y) > 200) ) {
                            JOptionPane.showMessageDialog(rootPane, "Introduceti numai valori in intervalul -200...200", "Atentie", JOptionPane.WARNING_MESSAGE);
                        } else {
                            Point p = transformare(new Point(x,y)); //coordonate locale
                            coordinateList.add(new Point(p.getX(),p.getY())); // coordonate globale
                        }
                    } catch (NumberFormatException exc) {
                        JOptionPane.showMessageDialog(rootPane, "Introduceti numai numere intregi", "Atentie", JOptionPane.WARNING_MESSAGE);
                    }
                }
                
            });
            
            
            introdPctMouse.setEnabled(true);
            introdPctCoord.setEnabled(true);
            sortare.setEnabled(true);
            acoperire.setEnabled(false);
            
            frame.setVisible(true);
        }
        
        private Point transformare(Point local) {
            //transformarea din coordonate locale in coordonate globale
            Point global = new Point(xPanel/2 + local.getX()-3,yPanel/2 - local.getY()-13);
            //de corelat cu frame-ul in care se vor afisa acestea
            local.printCoord();
            System.out.println();
            global.printCoord();
            return global;
        }
    }

    class IntrodListener extends JFrame implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (acop) {
                coordinateList.clear();
                acop = false;
            }
            
            setTitle("Introduceti punctele");
            setSize(xPanel,yPanel);
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            setVisible(true);
            setResizable(false);
            
            pointNumber--;
            pointNumber--;
            MousePanel panel = new MousePanel();
            setContentPane(panel);
            
            introdPctMouse.setEnabled(true);
            sortare.setEnabled(true);
            acoperire.setEnabled(false);
        }
        
        private class MousePanel extends JPanel implements MouseListener {
            private int x, y;

            public MousePanel(){
                super();
                addMouseListener(this);
                this.setSize(xPanel,yPanel);
            }

            @Override
            public void paintComponent(Graphics g) {
                //desenarea sistemului de coordonate local
                g.setColor(Color.BLACK);
                g.drawLine(0, this.getHeight()/2, this.getWidth(), this.getHeight()/2);
                g.drawLine(this.getWidth(), this.getHeight()/2, this.getWidth()-10,this.getHeight()/2-3);
                g.drawLine(this.getWidth(), this.getHeight()/2, this.getWidth()-10,this.getHeight()/2+3);
                
                g.drawLine(this.getWidth()/2, 0, this.getWidth()/2, this.getHeight());
                g.drawLine(this.getWidth()/2, 0, this.getWidth()/2-3,10);
                g.drawLine(this.getWidth()/2, 0, this.getWidth()/2+3,10);
                
                g.drawString("O", this.getWidth()/2-15, this.getHeight()/2-5);
                
                g.setColor(Color.BLUE);
                g.fillOval(x-3, y-3, 6, 6);
                g.drawString("" + pointNumber + "", x-5, y-5);
                pointNumber++;
            }

            @Override
            public void mouseClicked(MouseEvent mouse){ }

            @Override
            public void mouseEntered(MouseEvent mouse){ }   

            @Override
            public void mouseExited(MouseEvent mouse){ }

            @Override
            public void mousePressed(MouseEvent mouse){
                x = mouse.getX();
                y = mouse.getY();
                coordinateList.add(new Point(x,y));
                Point p = new Point(x,y);
                p.printCoord();
                System.out.println();
                repaint();
            }

            @Override
            public void mouseReleased(MouseEvent mouse){ }
        }
    }
    
    
    class SortareListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            introdPctMouse.setEnabled(false);
            introdPctCoord.setEnabled(false);
            sortare.setEnabled(false);
            acoperire.setEnabled(true);
            
            QuickSort quick = new QuickSort();
            quick.quicksort(coordinateList, 0, coordinateList.size()-1);
        }
    }
    
    
    class AcoperireListener implements ActionListener {
        
        
        @Override
        public void actionPerformed(ActionEvent e) {
            acop = true;
            
            introdPctMouse.setEnabled(true);
            introdPctCoord.setEnabled(true);
            sortare.setEnabled(true);
            acoperire.setEnabled(false);
            
            JFrame frame = new JFrame();
            frame.setResizable(false);
            
            
            frame.setTitle("Determinarea frontierei acoperirii convexe");
            frame.setSize(xPanel,yPanel);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.setVisible(true);
            
            GrahamsScan graham = new GrahamsScan(coordinateList);
            
            frame.setContentPane(graham);
            
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    graham.frontiera();
                }
            });
            
            thread.start();
            

            for (Point p : graham.getFront()) {
                p.printCoord();
                System.out.println();
            }
            
            pointNumber = 1;
            
            
        }
    }
}
