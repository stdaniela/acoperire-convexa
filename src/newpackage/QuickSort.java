package newpackage;

import java.util.ArrayList;

public class QuickSort {
    private int partitionare(ArrayList<Point> v, int stanga, int dreapta) {
        int i = stanga, j = dreapta;
        int pivot = v.get((stanga + dreapta) / 2).getX();
        
        while (i <= j) {
            while (v.get(i).getX() < pivot) {
                i++;
            }
            while (v.get(j).getX() > pivot) {
                j--;
            }
            if (i <= j) {
                Point tmp = v.get(i);
                v.set(i, v.get(j));
                v.set(j, tmp);
                i++;
                j--;
            }
        }
        return i;
    }
    
    public void quicksort(ArrayList<Point> v, int stanga, int dreapta) {
        int index = partitionare(v, stanga, dreapta);
        if (stanga < index - 1) {
            quicksort(v, stanga, index - 1);
        }
        if (index < dreapta) {
            quicksort(v, index, dreapta);
        }
    }
}

