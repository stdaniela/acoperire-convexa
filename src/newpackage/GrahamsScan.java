package newpackage;

import java.awt.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import javax.swing.*;

public class GrahamsScan extends JPanel {
    private ArrayList<Point> point = new ArrayList<>();
    private ArrayList<Point> front = new ArrayList<>();
    private boolean test;
    private String frontiera;
    
    public GrahamsScan(ArrayList<Point> p) {
        point = p;
    }
    
    
    public void setPoint(ArrayList<Point> p) {
        point = p;
    }
    
    public void setFront(ArrayList<Point> p) {
        front = p;
    }
    
    public ArrayList<Point> getFront() {
        return front;
    }
    
    @Override
    public void paintComponent(Graphics g) {
        g.clearRect(0, 0, 400, 400);
        
        //desenarea sistemului de coordonate local
        g.setColor(Color.BLACK);
        g.drawLine(0, this.getHeight()/2, this.getWidth(), this.getHeight()/2);
        g.drawLine(this.getWidth(), this.getHeight()/2, this.getWidth()-10,this.getHeight()/2-3);
        g.drawLine(this.getWidth(), this.getHeight()/2, this.getWidth()-10,this.getHeight()/2+3);
                
        g.drawLine(this.getWidth()/2, 0, this.getWidth()/2, this.getHeight());
        g.drawLine(this.getWidth()/2, 0, this.getWidth()/2-3,10);
        g.drawLine(this.getWidth()/2, 0, this.getWidth()/2+3,10);
                
        g.drawString("O", this.getWidth()/2-15, this.getHeight()/2-5);
        
        for (int count = 0; count < point.size(); count++) {
            if (front != null) {
                if (front.contains(point.get(count))) {
                    //punctul este pe frontiera
                    g.setColor(Color.GREEN);
                } else {
                    //punctul nu este pe frontiera
                    g.setColor(Color.MAGENTA);
                }
            } else {
                // inca nu am puncte pe frontiera
                g.setColor(Color.MAGENTA);
            }
            g.fillOval(point.get(count).getX()-3, point.get(count).getY()-3, 6, 6);
            g.drawString("" + (count + 1) + "", point.get(count).getX()-5, point.get(count).getY()-5);
        }
        
        
        switch (frontiera) {
            case "inf":
                g.setColor(Color.CYAN);
                break;
            case "sup":
                g.setColor(Color.RED);
                break;
            case "totala":
                g.setColor(Color.BLUE);
                break;
            default:
                g.setColor(Color.CYAN);
        }
        
        if (front != null) {
            if (test) {
                g.setColor(Color.BLUE);
            }
            for (int i = 1; i < front.size(); i++) {
                g.drawLine(front.get(i-1).getX(), front.get(i-1).getY(),
                        front.get(i).getX(), front.get(i).getY());
            }
        }
        
        if ( (test) && (front != null) ) {
            //am terminat si frontiera inferioara si frontiera superioara
            g.setColor(Color.BLUE);
            g.drawLine(front.get(front.size()-1).getX(), front.get(front.size()-1).getY(),
                    front.get(0).getX(), front.get(0).getY());
        }
    }
    
    //folosit pentru a determina daca am viraj la stanga sau la dreapta
    private int determinant(Point p, Point q, Point r) {
        int sum1 = q.getX()*r.getY() + p.getX()*q.getY() + r.getX()*p.getY();
        int sum2 = q.getX()*p.getY() + r.getX()*q.getY() + p.getX()*r.getY();
        return (sum1-sum2);
    }
    
    //provoaca o intarziere de x secunde
    private void timeDelay(int x) {
        try {
            TimeUnit.SECONDS.sleep(x);
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }
    
    //determinarea frontierei inferioare
    private ArrayList<Point> frontieraInf() {
        frontiera = "inf";
        
        ArrayList<Point> pctFrontInf = new ArrayList<>();
        pctFrontInf.add(point.get(0));
        pctFrontInf.add(point.get(1));
        
        front = pctFrontInf;
        repaint();
        timeDelay(1);
        
        for (int i = 2; i < point.size(); i++) {
            pctFrontInf.add(point.get(i));
            
            
            front = pctFrontInf;
            repaint();
            timeDelay(1);
            
            
            while ( (pctFrontInf.size() >= 3) && (determinant(pctFrontInf.get(pctFrontInf.size()-3),
                    pctFrontInf.get(pctFrontInf.size()-2),
                    pctFrontInf.get(pctFrontInf.size()-1)) >= 0) ) {
                pctFrontInf.remove(pctFrontInf.size()-2);
                
                
                front = pctFrontInf;
                repaint();
                timeDelay(1);
                
            }
        }
        
        return pctFrontInf;
    }
    
    //determinarea frontierei superioare
    private ArrayList<Point> frontieraSup() {
        frontiera = "sup";
        
        ArrayList<Point> pctFrontSup = new ArrayList<>();
        
        pctFrontSup.add(point.get(point.size() - 1));
        pctFrontSup.add(point.get(point.size() - 2));
        
        front = pctFrontSup;
        repaint();
        timeDelay(1);
        
        for (int i = point.size() - 3; i >= 0; i--) {
            pctFrontSup.add(point.get(i));
            
            front = pctFrontSup;
            repaint();
            timeDelay(1);
            
            while ( (pctFrontSup.size() >= 3) && (determinant(pctFrontSup.get(pctFrontSup.size()-3),
                    pctFrontSup.get(pctFrontSup.size()-2),
                    pctFrontSup.get(pctFrontSup.size()-1)) >= 0) ) {
                pctFrontSup.remove(pctFrontSup.size()-2);
                
                front = pctFrontSup;
                repaint();
                timeDelay(1);
            }
        }
        
        return pctFrontSup;
    }
    
    public void frontiera() {
        frontiera = "totala";
        
        ArrayList<Point> inf = frontieraInf();
        timeDelay(3);
        inf.remove(inf.size() - 1);
        ArrayList<Point> sup = frontieraSup();
        timeDelay(3);
        sup.remove(sup.size() - 1);
        ArrayList<Point> pctFront = new ArrayList<>();
        
        test = true;
        
        inf.stream().forEach((p1) -> {
            pctFront.add(p1);
        });
        
        sup.stream().forEach((p1) -> {
            pctFront.add(p1);
        });       
        
        setFront(pctFront);
        repaint();
    }
}

